package Home_java_projects;

import java.util.Scanner;

public class SimpleInterest {

	public static void main(String[] args) {

		double Principal, RateOfInterest, SimpleInterest;
		int NoOfYears;

		Scanner scan = new Scanner(System.in);

		System.out.print("\nEnter the principal amount : ");
		Principal = scan.nextDouble();

		System.out.print("\nEnter the rate of interest : ");
		RateOfInterest = scan.nextDouble();

		System.out.print("\nEnter no of years : ");
		NoOfYears = scan.nextInt();

		SimpleInterest = Principal * RateOfInterest * NoOfYears / 100;

		System.out.println("\nSimple interest is : " + SimpleInterest);

	}
}
