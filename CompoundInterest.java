package Home_java_projects;

import java.util.Scanner;

public class CompoundInterest {

	public static void main(String[] args) {
		while (true) {
			Scanner scan = new Scanner(System.in);

			double P, R, N, I, result;

			System.out.print("\nEnter the principal amount : ");
			P = scan.nextDouble();

			System.out.print("\nEnter the no of years : ");
			N = scan.nextDouble();

			System.out.print("\nEnter the rate of interest : ");
			R = scan.nextDouble();

			I = R / 100;

			result = P * Math.pow(1 + I, N);

			System.out.print("\nThe compound interest is " + result);
		}
	}
}
