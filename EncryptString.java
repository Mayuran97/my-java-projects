package Home_java_projects;

import java.util.Scanner;

public class EncryptString {

	 String realString;
	 String encryptedString;

	public String realString() {
		return realString;
	}
	public String encryptedString() {
		return encryptedString;
	}
	public void getInput() {
		Scanner scan = new Scanner(System.in);

		System.out.print("Enter the word : ");
		realString = scan.nextLine();
	}
	public String encrypt() {

		encryptedString = new String();
		encryptedString = "";

		char c;
		for (int i = 0; i < realString.length() - 1; i++) {

			c = realString.charAt(i);
			c = (char) ((int) c - 1);

			encryptedString += Character.toString(c);
		}
		return encryptedString;
	}

}
